async function preInit(inputObj) {

}

async function postInit(inputObj) {
    console.log(`\n    ______ _                         
    |  _  (_)                        
    | | | |_  __ _ _ __   __ _  ___  
    | | | | |/ _\` | '_ \\ / _\` |/ _ \\ 
    | |/ /| | (_| | | | | (_| | (_) |
    |___/ | |\\__,_|_| |_|\\__, |\\___/ 
         _/ |             __/ |      
        |__/             |___/       
                                        `)
    console.log(`\n    Welcome to the start-django application
     This application requires to open these services: 
         FC : https://fc.console.aliyun.com/
         ACR: https://cr.console.aliyun.com/
     This application can help you quickly deploy the Django project:
         Full yaml configuration: https://github.com/devsapp/django#%E5%AE%8C%E6%95%B4yaml
         Django development docs: https://docs.djangoproject.com/en/3.2/
     This application homepage: https://github.com/devsapp/start-django\n`)
}

module.exports = {
    postInit,
    preInit
}
