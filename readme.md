# 阿里云 Django 框架案例

## 快速体验

- 初始化项目：`s init devsapp/start-django`
- 进入项目后部署：`s deploy`
- 部署过程中可能需要阿里云密钥的支持，部署完成之后会获得到临时域名可供测试

动画演示：

![]( ./terminal.gif)

## 阿里云在线体验

[点击此处可以在线体验通过 Serverless Devs 部署 Django 系统到阿里云函数计算](https://api.aliyun.com/new#/tutorial?action=git_open&git_repo=https://github.com/devsapp/devsapp-cloudshell-example.git&tutorial=tutorial/start-django.md)

## 快速导航

- [示例Yaml](https://github.com/devsapp/django##示例Yaml)
	- [Yaml字段说明](https://github.com/devsapp/django##Yaml字段说明)
- [额外说明](https://github.com/devsapp/django##额外说明)
	- [NAS+Container模式](https://github.com/devsapp/django##NASContainer模式)
	- [Container模式](https://github.com/devsapp/django##Container模式)
	- [模式对比](https://github.com/devsapp/django##模式对比)
-----

> - Serverless Devs 项目：https://www.github.com/serverless-devs/serverless-devs   
> - Serverless Devs 文档：https://www.github.com/serverless-devs/docs   
> - Serverless Devs 钉钉交流群：33947367    